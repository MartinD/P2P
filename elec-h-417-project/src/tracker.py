import lib.server as ser
import lib.peer as pe
import lib.fileInfos as fi

FileInfos = fi.fileInfos()
chunksCount = FileInfos.chunks_count
filename = FileInfos.filename
filenameLength = len(filename)
hashes = FileInfos.get_chunks()
peers = FileInfos.get_peers()

Tracker = pe.peer()
Tracker.set_name('tracker')
ip = Tracker.find_ip()
port = Tracker.find_port()

UdpTracker = ser.udpTracker(9000)
UdpTracker.start()

TcpTracker = ser.tcpTracker(ip, port, chunksCount, filenameLength, filename, hashes, peers)
TcpTracker.connect_client()