import os
import sys
import filecmp
import configparser

root_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), '..')
files_path = os.path.join(root_path, 'files')
chunks_path = os.path.join(root_path, 'chunks')
config_path = os.path.join(root_path, 'config')

class CheckFile:
    def __init__(self):
        self.read_config_file()
        if self.check_file():
            print('Files are identical')
        else:
            print('Files are NOT identical')

    def read_config_file(self):
        config = configparser.ConfigParser()
        config.read(os.path.join(config_path, 'test.ini'))
        self.filename = config.get('description', 'filename')

    def check_file(self):
        source = os.path.join(files_path, self.filename)
        copy = os.path.join(chunks_path, 'charlie', self.filename)
        return filecmp.cmp(source, copy)

if __name__ == '__main__':
    CheckFile()
