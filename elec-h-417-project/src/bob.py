import socket
import threading
import lib.server as ser
import lib.peer as pe


Bob = pe.peer()
Bob.set_name('bob')
ip = Bob.find_ip()
port = Bob.find_port()
hashes = Bob.find_hashes()
folderName = Bob.find_folder_name()
Server = ser.serverPeer(ip, port, hashes, folderName)
Server.connect_client()
