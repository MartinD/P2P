import lib.server as ser
import lib.peer as pe


Alice = pe.peer()
Alice.set_name('alice')
ip = Alice.find_ip()
port = Alice.find_port()
hashes = Alice.find_hashes()
folderName = Alice.find_folder_name()
Server = ser.serverPeer(ip, port, hashes, folderName)
Server.connect_client()