#!/usr/bin/python3
# coding=utf-8
import sys
import lib.fileInfos as fi
import lib.peer as pe
import lib.client as cl

if len(sys.argv) < 2:
    print('Error: missing step number')
    sys.exit(1)
step = int(sys.argv[1])

FileInfos = fi.fileInfos()

if step == 1:
    print("\n########## STEP 1 ##########\n")
    Alice = pe.peer()
    Alice.set_name('alice')
    Bob = pe.peer()
    Bob.set_name('bob')

    ipAlice = Alice.find_ip()
    portAlice = Alice.find_port()
    hashesAlice = FileInfos.chunksPeers['alice']

    ipBob = Bob.find_ip()
    portBob = Bob.find_port()
    hashesBob = FileInfos.chunksPeers['bob']

    filename = FileInfos.get_filename()
    file = open('chunks/charlie/'+filename,"w")
    file.close()

    print(FileInfos.get_chunks_count(), "chunks to download")

    clientAlice = cl.client(ipAlice, portAlice, hashesAlice)
    clientAlice.start()

    clientBob = cl.client(ipBob, portBob, hashesBob)
    clientBob.start()

    FileInfos.set_filename(filename)
    FileInfos.set_hashes(FileInfos.get_chunks())

elif step == 2:
    print("\n########## STEP 2 ##########\n")
    Tracker = pe.peer()
    Tracker.set_name('tracker')

    ipTracker = Tracker.find_ip()
    portTracker = Tracker.find_port()

    clientTracker = cl.client(ipTracker, portTracker, 0)
    filename, peers, hashes = clientTracker.step2()
    file = open('chunks/charlie/' + filename, "w")
    file.close()

    FileInfos.set_filename(filename)
    FileInfos.set_hashes(hashes)

    print(len(hashes), "chunks to download for the file", filename)

    for peer in peers:
        clientPeer = cl.client(peer.get_ip(), peer.get_port(), peer.get_hashes())
        clientPeer.start()

elif step == 3:
    print("\n########## STEP 3 ##########\n")
    clientFindTracker = cl.client(0,0,0)
    ipTracker, portTracker = clientFindTracker.step3()

    clientTracker = cl.client(ipTracker, portTracker, 0)
    filename, peers, hashes = clientTracker.step2()
    file = open('chunks/charlie/' + filename, "w")
    file.close()

    print(len(hashes), " chunks to download for the file", filename)

    for peer in peers:

        print(peer.get_ip(), peer.get_port())
        clientPeer = cl.client(peer.get_ip(), peer.get_port(), peer.get_hashes())
        clientPeer.start()

else:
    print('Error: invalid step number')
    sys.exit(1)
