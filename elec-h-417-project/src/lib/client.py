import socket
import lib.protocols as prot
import lib.messages as mess
import threading
import time
import os


Protocol = prot.protocols()
Message = mess.messages()


class client(threading.Thread):

    def __init__(self, ip, port, hashes):
        super(client, self).__init__()
        self.ip = ip
        self.port = port
        self.hashes = hashes


    def run(self):
        self.step1()


    def step1(self):
        serverAddress = (self.ip, self.port)
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        sock.connect(serverAddress)
        print("\nCONNEXION OPENED ===>", serverAddress)
        print(serverAddress, " has ", len(self.hashes), " chunks\n")
        for hash in self.hashes:
            filename = "chunks/charlie/"+ hash + ".bin"
            Message.send_msg(Protocol.GET_CHUNK(hash), sock)
            msg = Message.receive_msg(sock)
            type, length = Protocol.DECODE_HEADER(msg)
            if type == 5:
                msg = Message.receive_all_msg(sock, msg, length)
                if not os.path.isfile(filename):
                    file = open(filename, 'wb')
                    file.write(Protocol.DECODE_CHUNK(msg))
                    file.close()
                    print("Chunk:",hash+".bin downloaded from", serverAddress)
                else:
                    print("file already exists")
            elif type == 6:
                if(Protocol.DECODE_ERROR(msg) == 0):
                    print("INVALID_MESSAGE_FORMAT")
                if (Protocol.DECODE_ERROR(msg) == 1):
                    print("INVALID_REQUEST")
                if (Protocol.DECODE_ERROR(msg) == 2):
                    print("CHUNK_NOT_FOUND")
            else:
                print("NOT RECOGNISED")

        sock.close()
        print("\nCONNEXION CLOSED ===>", serverAddress, "\n")


    def step2(self):

        serverAddress = (self.ip, self.port)
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        sock.connect(serverAddress)

        while True:
            Message.send_msg(Protocol.GET_FILE_INFO(), sock)
            msg = Message.receive_msg(sock)
            type, length = Protocol.DECODE_HEADER(msg)

            if type == 3:
                msg = Message.receive_all_msg(sock, msg, length)
                filename, peers, hashes = Protocol.DECODE_FILE_INFO(msg)
                sock.close()
                return filename, peers, hashes
            elif type == 6:
                if (Protocol.DECODE_ERROR(msg) == 0):
                    print("INVALID_MESSAGE_FORMAT")
                if (Protocol.DECODE_ERROR(msg) == 1):
                    print("INVALID_REQUEST")
                if (Protocol.DECODE_ERROR(msg) == 2):
                    print("CHUNK_NOT_FOUND")
            else:
                print("NOT RECOGNISED")

    def step3(self):
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)

        while True:
            sock.sendto(Protocol.DISCOVER_TRACKER(), ('<broadcast>', 9000))
            msg, address = Message.receive_msg_from(sock)
            if Message.format(msg) == 1:
                ip, port = Protocol.DECODE_TRACKER_INFO(msg)
                return ip, port
            elif Message.format(msg) == 6:
                if (Protocol.DECODE_ERROR(msg) == 0):
                    print("INVALID_MESSAGE_FORMAT")
                if (Protocol.DECODE_ERROR(msg) == 1):
                    print("INVALID_REQUEST")
                if (Protocol.DECODE_ERROR(msg) == 2):
                    print("CHUNK_NOT_FOUND")
            else:
                print("NOT RECOGNISED")
            time.sleep(2)