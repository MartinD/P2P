import threading
import socket
import threading
import lib.protocols as prot
import lib.messages as mess
import time

Message = mess.messages()
Protocol = prot.protocols()

class serverPeer:

    def __init__(self, ip, port, hashes, folderName):
        self.hashes = hashes
        self.folderName = folderName
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.sock.bind((ip, port))
        self.sock.listen()
        print("Server listening at port", port)

    def handle_client(self, client_sock, client_addr):
        while True:
            try:
                msg = Message.receive_msg(client_sock)
                if len(msg) < 1:
                    break
                elif Message.format(msg) == -1:
                    print("INVALID_MESSAGE_FORMAT")
                    Message.send_msg(Protocol.ERROR(0), client_sock)
                elif Message.format(msg) != 4:
                    print("INVALID_REQUEST")
                    Message.send_msg(Protocol.ERROR(1), client_sock)
                elif Message.format(msg) == 4:
                    print("CHUNK FOUND")
                    hash = Protocol.DECODE_GET_CHUNK(msg)
                    if hash in self.hashes:
                        msg = Protocol.CHUNK(hash, self.folderName)
                        Message.send_msg(msg, client_sock)
                    else:
                        print("CHUNK_NOT_FOUND")
                        Message.send_msg(Protocol.ERROR(2), client_sock)
            except IndexError:
                print("INDEX ERROR : CONNEXION ABORTED")
                break

    def connect_client(self):
        while True:
            client_sock, client_addr = self.sock.accept()
            th = threading.Thread(target=self.handle_client, args=(client_sock, client_addr))
            th.daemon = True
            th.start()


class tcpTracker:

    def __init__(self, ip, port, chunksCount, filenameLength, filename, hashes, peers):
        self.chunksCount = chunksCount
        self.filenameLength = filenameLength
        self.filename = filename
        self.hashes = hashes
        self.peers = peers
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.sock.bind((ip, port))
        self.sock.listen()
        print("Server listening at port", port)

    def handle_client(self, client_sock, client_addr):
        while True:
            try:
                msg = Message.receive_msg(client_sock)
                if len(msg) < 1:
                    break
                elif Message.format(msg) == -1:
                    print("INVALID_MESSAGE_FORMAT")
                    Message.send_msg(Protocol.ERROR(0), client_sock)
                elif Message.format(msg) != 2:
                    print("INVALID_REQUEST")
                    Message.send_msg(Protocol.ERROR(1), client_sock)
                elif Message.format(msg) == 2:
                    print("SEND FILE INFO")
                    Message.send_msg(Protocol.FILE_INFO(self.chunksCount, self.filename,
                                                        self.hashes, self.peers), client_sock)
            except IndexError:
                print("INDEX ERROR : CONNEXION ABORTED")
                break

    def connect_client(self):
        while True:
            client_sock, client_addr = self.sock.accept()
            th = threading.Thread(target=self.handle_client, args=(client_sock, client_addr))
            th.daemon = True
            th.start()


class udpTracker(threading.Thread):

    def __init__(self, port):
        super(udpTracker, self).__init__()
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        self.sock.bind(('', port))
        print("Server listening at port", port)

    def run(self):
        self.connect_client()

    def connect_client(self):
        while True:
            msg, address = Message.receive_msg_from(self.sock)
            if Message.format(msg) == 0:
                Message.send_msg_to(Protocol.TRACKER_INFO(), self.sock, address)