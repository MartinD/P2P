#!/usr/bin/python3
# -*- coding:Utf-8 -*-
import lib.protocols as prot

DISCOVER_TRACKER = 0
TRACKER_INFO = 1
GET_FILE_INFO = 2
FILE_INFO = 3
GET_CHUNK = 4
CHUNK = 5
ERROR = 6
types = [DISCOVER_TRACKER, TRACKER_INFO, GET_FILE_INFO, FILE_INFO, GET_CHUNK, CHUNK, ERROR]

Protocol = prot.protocols()

class messages:

    def format(self, msg):
        type, length = Protocol.DECODE_HEADER(msg)
        if type not in types:
            return -1
        else:
            return type

    def send_msg(self, msg, sock):
        sock.send(msg)

    def send_msg_to(self, msg, sock, clientAddr):
        sock.sendto(msg, clientAddr)

    def receive_msg(self, sock):
        return sock.recv(2048)

    def receive_all_msg(self, sock, msg, length):
        while len(msg) < length*4:
            packet = sock.recv(length*4-len(msg))
            if not packet:
                return None
            msg += packet
        return msg

    def receive_msg_from(self, sock):
        return sock.recvfrom(2087152)
