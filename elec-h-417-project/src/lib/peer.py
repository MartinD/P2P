# !/usr/bin/python3
import os
import configparser

config = configparser.ConfigParser()
folderPath = os.path.join(os.path.dirname(os.path.realpath(__file__)),'../..')
configPath = os.path.join(folderPath, 'config')

class peer:

    def __init__(self):
        self.ip = ''
        self.port = 0
        self.hashes = []

    def find_folder_name(self):
        return os.path.join(folderPath, 'chunks/' + self.name)

    def find_ip(self):
        config.read(os.path.join(configPath, 'peers.ini'))
        return config.get(self.name, 'ip_address')

    def find_port(self):
        config.read(os.path.join(configPath, 'peers.ini'))
        return int(config.get(self.name, 'port_number'))

    def find_hashes(self):
        hashes = os.popen('ls '+self.find_folder_name()+'/*.bin').read().strip('\n').split('\n')
        for i in range(len(hashes)):
            hashes[i] = hashes[i][-44:-4]
        return hashes

    def set_name(self, name):
        self.name = name

    def set_ip(self, ip):
        self.ip = ip

    def set_port(self, port):
        self.port = int(port)

    def add_hash(self, hash):
        self.hashes.append(hash)

    def get_ip(self):
        return self.ip

    def get_port(self):
        return self.port

    def get_hashes(self):
        return self.hashes