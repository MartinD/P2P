#!/usr/bin/python3
# -*- coding:Utf-8 -*-
import struct
import binascii     # used to convert the hash string (in hexadecimal) to 20 bytes
import os
import configparser
import lib.peer as pe

config = configparser.ConfigParser()

class protocols():

    def protocol(self, type, length, data):
        """
        add the header to the data in the input. We get the entire message as output
        :param type: integer
        :param length: integer
        :param data: bytes
        :return: bytes
        """
        self.header = struct.pack("!bbhi", 1, type, 0, length) # 1 = version (always 1) 0 = reserved (random, not used)
        self.data = data
        self.content = self.header + self.data
        return self.content

    def HEADER(self, type, length):
        return struct.pack("!bbhi", 1, type, 0, length)

    def DECODE_HEADER(self, msg):
        """
        take the type and the length from the message
        :param msg: bytes
        :return: (integer, integer)
        """
        msg = struct.unpack("!bbhi", msg[:8])
        type, length = msg[1], msg[3]
        return type, length

    def GET_CHUNK(self, hash):
        data = bytearray(binascii.a2b_hex(hash))
        return self.protocol(4, 7, data)

    def DECODE_GET_CHUNK(self, msg):
        hash = binascii.b2a_hex(msg[8:]).decode("utf-8")
        return hash

    def CHUNK(self, hash, path):
        chunkPath = os.path.join(path,hash+".bin")
        chunkFile = open(chunkPath,"rb")
        chunkContent = chunkFile.read()
        chunkContentPadded = self.add_padding(chunkContent)
        chunkContentLength = len(chunkContent)
        chunkContentLengthPadded = len(chunkContentPadded)
        chunkFile.close()

        data = bytearray(binascii.a2b_hex(hash))
        fmt = "!i%ds" % chunkContentLengthPadded
        data += struct.pack(fmt, chunkContentLength, chunkContentPadded)
        return self.protocol(5, (len(data)+8)//4, data)

    def DECODE_CHUNK(self, msg):
        (chunkContentLength,) = struct.unpack('!i', msg[28:32])
        fmt = '!%ds' % chunkContentLength
        (chunkContent,) = struct.unpack(fmt, msg[32:32+chunkContentLength])
        return chunkContent

    def ERROR(self, code):
        data = struct.pack("!hh", code, 0)
        return self.protocol(6, 3, data)

    def DECODE_ERROR(self, msg):
        (code,) = struct.unpack('!h', msg[8:10])
        return code

    def GET_FILE_INFO(self):
        return self.HEADER(2, 2)

    def DECODE_GET_FILE_INFO(self, msg):
        type, length = self.DECODE_HEADER(msg)
        return type

    def FILE_INFO(self, chunksCount, filename, hashes, peers):
        config = configparser.ConfigParser()
        folderPath = os.path.join(os.path.dirname(os.path.realpath(__file__)), '../..')
        configPath = os.path.join(folderPath, 'config')
        config.read(os.path.join(configPath, 'peers.ini'))
        filename = filename.encode("utf-8")
        filenameLength = len(filename)
        fmt1 = "!hh%ds" % filenameLength
        data = struct.pack(fmt1, int(chunksCount), filenameLength, filename)
        data = self.add_padding(data)
        for i in range(len(hashes)):
            data += bytearray(binascii.a2b_hex(hashes[i]))
            peersCount = len(peers[i])
            data += struct.pack("!hh", peersCount, 0)
            for j in range(peersCount):
                ipAddress = config.get(peers[i][j], 'ip_address')
                portNumber = int(config.get(peers[i][j], 'port_number'))
                data += self.encode_ip(ipAddress)
                data += struct.pack("!hh", portNumber, 0)
        return self.protocol(3, (len(data)+8)//4, data)

    def DECODE_FILE_INFO(self, msg):
        chunksCount, filenameLength = struct.unpack("!hh", msg[8:12])
        fmt = "!%ds" % filenameLength
        (filename,) = struct.unpack(fmt, msg[12:12+filenameLength])
        filename = filename.decode("utf-8")
        filenameLengthPadded = self.length_padding(filenameLength)
        peers = []
        pos = 12 + filenameLengthPadded
        hashes = []
        for i in range(int(chunksCount)):
            hash = binascii.b2a_hex(msg[pos:pos+20]).decode("utf-8")
            hashes.append(hash)
            pos += 20
            peersCount, padding = struct.unpack('!hh', msg[pos:pos+4])
            pos += 4
            for j in range(peersCount):
                ipAddress = self.decode_ip(msg[pos:pos+4])
                pos+=4
                (portNumber,) = struct.unpack('!h', msg[pos:pos+2])
                pos += 4
                if len(peers) < 1:
                    peers.append(self.new_peer(ipAddress, portNumber, hash))
                elif ipAddress in [peer.get_ip() for peer in peers] and portNumber in [peer.get_port() for peer in peers]:
                    peers[self.old_peer(peers, ipAddress, portNumber)].add_hash(hash)
                else:
                    peers.append(self.new_peer(ipAddress, portNumber, hash))
        return filename, peers, hashes

    def DISCOVER_TRACKER(self):
        return self.HEADER(0, 2)

    def TRACKER_INFO(self):
        config = configparser.ConfigParser()
        folderPath = os.path.join(os.path.dirname(os.path.realpath(__file__)), '../..')
        configPath = os.path.join(folderPath, 'config')
        config.read(os.path.join(configPath, 'peers.ini'))
        ipAddress =  config.get('tracker', 'ip_address')
        ipAddress = self.encode_ip(ipAddress)
        portNumber = config.get('tracker', 'port_number')
        name = config.get('tracker', 'name')
        name = name.encode("utf-8")
        nameLength = len(name)
        fmt = '!hh%ds' % nameLength
        data = ipAddress
        data += struct.pack(fmt, int(portNumber), nameLength, name)
        data = self.add_padding(data)
        return self.protocol(1, (len(data)+8)//4, data)

    def DECODE_TRACKER_INFO(self, msg):
        ip = self.decode_ip(msg[8:12])
        (port,) = struct.unpack('!h', msg[12:14])
        return ip, port

    def add_padding(self, data):
        while len(data)%4 != 0:
            data += b'\x00'
        return data

    def length_padding(self, length):
        while length % 4 != 0:
            length += 1
        return length

    def encode_ip(self, ip_str):
        b = bytearray()
        for element in ip_str.split('.'):
            b += int(element).to_bytes(1, byteorder="big")
        return b

    def decode_ip(self, ip_bytearray):
        lst_number = []
        for element in ip_bytearray:
            lst_number.append(str(element))
        return '.'.join(lst_number)

    def new_peer(self, ip, port, hash):
        Peer = pe.peer()
        Peer.set_ip(ip)
        Peer.set_port(port)
        Peer.add_hash(hash)
        return Peer

    def old_peer(self, peers, ip, port):
        for i in range(len(peers)):
            if peers[i].ip == ip and peers[i].port == port:
                return i
