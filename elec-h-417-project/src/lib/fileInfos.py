# !/usr/bin/python3
import os
import configparser

config = configparser.ConfigParser()
rawConfig = configparser.RawConfigParser()
folderPath = os.path.join(os.path.dirname(os.path.realpath(__file__)),'../..')
configPath = os.path.join(folderPath, 'config')

class fileInfos:

    def __init__(self):
        self.filename = self.get_filename()
        self.chunks_count = self.get_chunks_count()
        self.chunksPeers = self.get_chunks_peers()
        self.chunks = self.get_chunks()
        self.peers = self.get_peers()

    def get_filename(self):
        config.read(os.path.join(configPath, 'file.ini'))
        return config.get('description', 'filename')

    def get_chunks_count(self):
        config.read(os.path.join(configPath, 'file.ini'))
        return config.get('description', 'chunks_count')

    def get_chunks_peers(self):
        chunksNames = {}
        for i in range(int(self.chunks_count)):
            listName = config.get('chunks_peers', str(i)).split(', ')
            for name in listName:
                if name not in chunksNames.keys():
                    chunksNames[name] = []
                chunksNames[name].append(config.get('chunks', str(i)))
        return chunksNames

    def get_chunks(self):
        chunks = []
        for i in range(int(self.chunks_count)):
            chunks.append(config.get('chunks', str(i)))
        return chunks

    def get_peers(self):
        peers = []
        for i in range(int(self.chunks_count)):
            peers.append(config.get('chunks_peers', str(i)).split(', '))
        return peers

    def set_filename(self, filename):
        rawConfig.add_section('description')
        rawConfig.set('description', 'filename', filename)
        with open('config/test.ini', 'w') as configFile:
            rawConfig.write(configFile)

    def set_hashes(self, hashes):
        rawConfig.add_section('chunks')
        rawConfig.set('description', 'chunks_count', str(len(hashes)))
        for i in range(len(hashes)):
            rawConfig.set('chunks', str(i), hashes[i])
        with open('config/test.ini', 'w') as configFile:
            rawConfig.write(configFile)
        """
        for peer in peers:
            for hash in peer.get_hashes():
                if hash not in hashes:
                    hashes.append(hash)
        for i in  range(len(hashes)):
            rawConfig.set('chunks', str(i), hashes[i])
        rawConfig.set('description', 'chunks_count', str(len(hashes)))
        with open ('config/test.ini', 'w') as configFile:
            rawConfig.write(configFile)
        """
